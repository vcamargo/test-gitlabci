import org.junit.Assert;
import org.junit.Test;

public class HelloWorldTest {
    public static final String EXPECTED_VALUE = "HelloWorld";

    @Test
    public void helloWorld() {
        Assert.assertEquals(EXPECTED_VALUE, "HelloWorld");
    }
}
